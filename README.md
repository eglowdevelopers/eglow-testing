# Eglow Testing

## Instalation

Follow the next instruction in that order

- Rename the file ```env.example``` to ```.env```
- run ```composer install```
- run ```php artisan migrate```
- run ```php artisan db:seed```
- run ```php artisan serve```

----

# APIs

## Influencers List

Returns json data about all Influencers.

* **URL**

  http://host/api/influencers

* **Method:**

  `GET`

* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": [
        {
            "id": 1,
            "social_network_id": 1,
            "category_id": 1,
            "username": "carlossantaana",
            "full_name": "Carlos Santa Ana",
            "phone": "9611882821",
            "email": "santana_c@eglow.com",
            "followers_count": 2000,
            "following_count": 6000,
            "created_at": "2020-12-21 23:48:27",
            "updated_at": "2020-12-21 23:48:27",
            "category": {
                "id": 1,
                "name": "Foodie",
                "is_active": 1,
                "created_at": null,
                "updated_at": null
            },
            "social_network": {
                "id": 1,
                "name": "Facebook",
                "is_active": 1,
                "created_at": null,
                "updated_at": null
            }
        }
    ]
}
```

---

## Influencer

Returns json data about a single Influencer.

* **URL**

  http://host/api/influencers/{id}

* **Method:**

  `GET`

*  **URL Params**

   ```
   id=[integer]
   ```
  
* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": {
        "id": 1,
        "social_network_id": 1,
        "category_id": 1,
        "username": "_carlosrivera",
        "full_name": "Carlos Rivera",
        "phone": "9611882821",
        "email": "_carlosrivera@gmail.com",
        "followers_count": 6300000,
        "following_count": 1372,
        "created_at": "2020-12-21 23:48:27",
        "updated_at": "2020-12-21 23:48:27",
        "category": {
            "id": 1,
            "name": "Foodie",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        "social_network": {
            "id": 1,
            "name": "Facebook",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        }
    }
}
```
 
* **Error Response:**

* **Code:** 404 NOT FOUND
  **Content:** `{ error : "Influencer not found" }`

---

## Store Influencer

Store data about a single Influencer.

* **URL**

  http://host/api/influencers

* **Method:**

  `POST`

*  **URL Params**

```
   id=[integer]
   social_network_id=[required]
   category_id=[required]
   username=[required, max:50, unique]
   full_name=[required, max:255]
   phone=[required, max:10, min:10]
   email=[required, max:100, unique]
   followers_count=[required, amount min:0]
   following_count=[required, amount min:0]
```
  
* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": {
        "social_network_id": "3",
        "category_id": "4",
        "username": "dannapaola",
        "full_name": "Danna Paola",
        "phone": "5510022910",
        "email": "dannapaola@gmail.com",
        "followers_count": "28000000",
        "following_count": "1073",
        "updated_at": "2020-12-22 00:19:14",
        "created_at": "2020-12-22 00:19:14",
        "id": 2
    }
}
```
 
* **Error Response:**

* **Code:** 422 Unprocessable Entity
**Content:**
```
{
    "message": "The request could not be processed, incorrect parameters.",
    "errors": {
        "social_network_id": [
            "The social network id field is required."
        ],
        "category_id": [
            "The category id field is required."
        ],
        "username": [
            "The username field is required."
        ],
        "full_name": [
            "The full name field is required."
        ],
        "phone": [
            "The phone field is required."
        ],
        "email": [
            "The email field is required."
        ],
        "followers_count": [
            "The followers count field is required."
        ],
        "following_count": [
            "The following count field is required."
        ]
    }
}
```

---

## Update Influencer

update data about a single Influencer.

* **URL**

  http://host/api/influencers/{id}

* **Method:**

  `PATCH/PUT`

*  **URL Params**

   ```
   id=[integer]
   ```

*  **Required:**
 
```
   id=[integer]
   social_network_id=[required]
   category_id=[required]
   username=[required, max:50, unique]
   full_name=[required, max:255]
   phone=[required, max:10, min:10]
   email=[required, max:100, unique]
   followers_count=[required, amount min:0]
   following_count=[required, amount min:0]
```
  
* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": {
        "id": 2,
        "social_network_id": "3",
        "category_id": "4",
        "username": "dannapaola",
        "full_name": "Danna Paola",
        "phone": "5510022910",
        "email": "dannapaola@gmail.com",
        "followers_count": "28000000",
        "following_count": "190",
        "created_at": "2020-12-22 00:19:14",
        "updated_at": "2020-12-22 00:23:50"
    }
}
```
 
* **Error Response:**

* **Code:** 422 Unprocessable Entity
**Content:**
```
{
    "message": "The request could not be processed, incorrect parameters.",
    "errors": {
        "social_network_id": [
            "The social network id field is required."
        ],
        "category_id": [
            "The category id field is required."
        ],
        "username": [
            "The username field is required."
        ],
        "full_name": [
            "The full name field is required."
        ],
        "phone": [
            "The phone field is required."
        ],
        "email": [
            "The email field is required."
        ],
        "followers_count": [
            "The followers count field is required."
        ],
        "following_count": [
            "The following count field is required."
        ]
    }
}
```

OR

* **Code:** 404 NOT FOUND
    **Content:** `{ error : "Influencer not found." }`


## Delete Influencers

Delete an Influencer of platform.

* **URL**

  http://host/api/influencers/{id}

* **Method:**

  `DELETE`

*  **URL Params**

```
id=[integer]
```
  
* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": "Influencer deleted"
}
```
 
* **Error Response:**

* **Code:** 404 NOT FOUND
**Content:** `{ error : "Influencer not found" }`

## Categories List

Returns json data about all Categories.

* **URL**

  http://host/api/categories

* **Method:**

  `GET`

* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Foodie",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 2,
            "name": "Telecommunications",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 3,
            "name": "Applications",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 4,
            "name": "Travel",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 5,
            "name": "Entertainment",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 6,
            "name": "Cinema and TV",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        }
    ]
}
```

---

## Social Networks List

Returns json data about all Categories.

* **URL**

  http://host/api/social-networks

* **Method:**

  `GET`

* **Success Response:**

* **Code:** 200
*  **Content:**
```
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Facebook",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 2,
            "name": "Instagram",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 3,
            "name": "Tik Tok",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 4,
            "name": "YouTube",
            "is_active": 1,
            "created_at": null,
            "updated_at": null
        }
    ]
}
```
