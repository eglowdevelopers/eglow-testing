<?php

namespace App\Http\Controllers;

use App\Models\SocialNetwork;
use Illuminate\Http\Request;

class SocialNetworkController extends Controller
{
    public function index()
    {
        $socialNetworks = SocialNetwork::all();
        return response(['success' => true, 'data' => $socialNetworks]);
    }
}
