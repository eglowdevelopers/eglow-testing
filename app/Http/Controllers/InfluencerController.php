<?php

namespace App\Http\Controllers;

use App\Models\Influencer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

// use Validator;

class InfluencerController extends Controller
{

    protected $rules = [
        'social_network_id' => 'required',
        'category_id' => 'required',
        'username' => 'required|max:50|unique:influencers,username',
        'full_name' => 'required|max:255',
        'phone' => 'required|max:10|min:10',
        'email' => 'required|max:100|unique:influencers,email',
        'followers_count' => 'required|numeric|min:0',
        'following_count' => 'required|numeric|min:0'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $influencers = Influencer::with('category')->get();
        return response(['success' => true, 'data' => $influencers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);

        if ( $validator->fails() ) {
            return response()->json([
                'message' => 'The request could not be processed, incorrect parameters.',
                'errors'  => $validator->errors()
            ], 422);
        }

        $influencer = new Influencer();
        $influencer->fill($request->all());
        $influencer->save();

        return response(['success' => true, 'data' => $influencer]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $influencer = Influencer::with('category', 'socialNetwork')->find($id);
        if (!empty($influencer)) {
            return response(['success' => true, 'data' => $influencer]);
        }
        return response(['message' => 'Influencer not found'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->rules;
        $rules['username'] = $rules['username'] . ',' . $id;
        $rules['email']    = $rules['email'] . ',' . $id;
        
        $validator = Validator::make($request->all(), $rules);
        
        if ( $validator->fails() ) {
            return response()->json([
                'message' => 'The request could not be processed, incorrect parameters.',
                'errors'  => $validator->errors()
            ], 422);
        }

        $influencer = Influencer::find($id);
        if (!empty($influencer)) {
            if ($influencer->update($request->all())) {
                return response(['success' => true, 'data' => $influencer]);
            } else {
                return response(['message' => 'The Influencer was not updated'], 500);
            }
        }
        return response(['message' => 'Influencer not found'], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $influencer = Influencer::find($id);
        if (!empty($influencer)) {
            if (Influencer::destroy($id)) {
                return response(['success' => true, 'data' => 'Influencer deleted']);
            } else {
                return response(['message' => 'The Influencer was not deleted'], 500);
            }
        }
        return response(['message' => 'Influencer not found'], 404);
    }
}
