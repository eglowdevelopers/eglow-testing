<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Influencer extends Model
{
    protected $table = 'influencers';

    protected $fillable = [
        'social_network_id',
        'category_id',
        'username',
        'full_name',
        'phone',
        'email',
        'followers_count',
        'following_count'
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function socialNetwork()
    {
        return $this->hasOne(SocialNetwork::class, 'id', 'social_network_id');
    }
}
