<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialNetworskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_networks')->insert([
            'name'      => 'Facebook',
            'is_active' => true
        ]);
        DB::table('social_networks')->insert([
            'name'      => 'Instagram',
            'is_active' => true
        ]);
        DB::table('social_networks')->insert([
            'name'      => 'Tik Tok',
            'is_active' => true
        ],[
            'name'      => 'YouTube',
            'is_active' => true
        ]);
        DB::table('social_networks')->insert([
            'name'      => 'YouTube',
            'is_active' => false
        ]);
    }
}
