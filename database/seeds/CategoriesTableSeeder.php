<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name'      => 'Foodie',
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'name'      => 'Telecommunications',
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'name'      => 'Applications',
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'name'      => 'Travel',
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'name'      => 'Entertainment',
            'is_active' => true
        ]);
        DB::table('categories')->insert([
            'name'      => 'Cinema and TV',
            'is_active' => true
        ]);
    }
}
